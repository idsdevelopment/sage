﻿using System;
using System.IO;
using IDS_Sage.IDS.Http;
using IDS_Sage.Import;

namespace IDS_Sage.IDS
{
	internal class Communications
	{
		private string CarrierId, AccountId, UserId, Password, ComsLink, StartingInvoice;

#if DEBUG
		const string Link = "http://jbtest.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";
#else
		const string Link = "http://users.internetdispatcher.org:8080/ids/reports?carrierId=%CID%&accountId=%ACID%&userId=%UID%&p=%PASS%&report=%REPORT%&output=99&invoiceId=%INV_ID%&targetDriverId=&targetStaffId=&targetAccountId=&from=&to=&fromStatus=1&toStatus=1&LOCALE=en_CA&zoneAllKey=ALL&zones=ALL&skipSessionTrips=true";
#endif

		internal Communications( string carrierId, string accountId, string userId, string password, int lastInvoiceNumber, string provider )
		{
			CarrierId = carrierId;
			AccountId = accountId;
			UserId = userId;
			Password = password;
			StartingInvoice = lastInvoiceNumber.ToString();

			ComsLink = Link.Replace( "%CID%", carrierId )
						   .Replace( "%ACID%", accountId )
						   .Replace( "%UID%", userId )
						   .Replace( "%PASS%", password )
						   .Replace( "%INV_ID%", StartingInvoice )
						   .Replace( "%REPORT%", provider );
		}

		internal void GetCsv( Action<Stream> onSuccess, Action<string> onFail )
		{
			string Csv = "";
			bool ReplyError = false;
			String Reason = "";

			HttpIo.WebIo.Get( new HttpIo.RequestEntry(	ComsLink, 
														( HttpIo.RequestEntry Reply ) =>
														{
															Csv = Reply.Content;
														},
														( HttpIo.RequestEntry Reply ) =>
														{
															ReplyError = true;
															Reason = Reply.Content.Trim();
														},
														true
													 )
							);

			if( ReplyError || Csv.Contains( Reason = "Authentication Error" ) )
				onFail( Reason );
			else
				onSuccess( new MemoryStream( System.Text.Encoding.UTF8.GetBytes( Csv ) ) );
		}
	}

}
