﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;


namespace IDS_Sage
{
	internal class CsvReader : List<CsvReader.Columns>
	{
		internal class Columns : List<String>
		{
		}

		internal CsvReader( Stream fileStream )
		{
			MemoryStream TempStream = new MemoryStream();
			fileStream.Position = 0;
			fileStream.CopyTo( TempStream );

			TempStream.Position = 0;

			using( var SReader = new StreamReader( TempStream ) )
			{
				using( var CReader = new CsvHelper.CsvReader( SReader ) )
				{
					CReader.Configuration.HasHeaderRecord = false;

					while( CReader.Read() )
					{
						var NewRow = new CsvReader.Columns();

						int I = 0;
						String Field;

						while( CReader.TryGetField<String>( I++, out Field ) )
							NewRow.Add( Field );

						Add( NewRow );
					}
				}
			}
		}

		internal CsvReader( String fileName )
			: this( new FileStream( fileName, FileMode.Open, FileAccess.Read ) )
		{
		}

		internal void WriteToStream( Stream stream )
		{
			using( var SWriter = new StreamWriter( stream ) )
			{
				using( var CWriter = new CsvHelper.CsvWriter( SWriter ) )
				{
					CWriter.Configuration.HasHeaderRecord = false;

					foreach( var Row in this )
					{
						foreach( var Col in Row )
							CWriter.WriteField( Col );

						CWriter.NextRecord();
					}
				}
			}
		}
	}
}
