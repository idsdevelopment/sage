﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace IDS_Sage
{
	internal static class  Utils
	{
		internal static String AddPathSeparator( String FullPath )
		{
			return( FullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar ) + Path.DirectorySeparatorChar );
		}


		internal static bool ChangeConnectionString( string AppName, string dbName, string databasePathName, string providerName = "" )
		{
			bool retVal = false;
			try
			{
				string FILE_NAME = string.Concat( Application.StartupPath, "\\", AppName.Trim(), ".Config" ); //the application configuration file name
				XmlTextReader reader = new XmlTextReader( FILE_NAME );
				XmlDocument doc = new XmlDocument();
				doc.Load( reader );
				reader.Close();
				string nodeRoute = string.Concat( "connectionStrings/add" );

				XmlNode cnnStr = null;
				XmlElement root = doc.DocumentElement;
				XmlNodeList Settings = root.SelectNodes( nodeRoute );

				for( int i = 0; i < Settings.Count; i++ )
				{
					cnnStr = Settings[ i ];
					if( cnnStr.Attributes[ "name" ].Value.Equals( dbName ) )
						break;
					cnnStr = null;
				}

				cnnStr.Attributes[ "connectionString" ].Value = databasePathName;
				if( providerName != "" )
						cnnStr.Attributes[ "providerName" ].Value = providerName;
				doc.Save( FILE_NAME );
				retVal = true;
			}
			catch
			{
				retVal = false;
			}
			return retVal;
		}
	}
}
