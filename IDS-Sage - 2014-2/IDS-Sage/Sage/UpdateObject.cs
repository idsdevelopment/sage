﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace IDS_Sage.Import
{
	internal partial class Ids_Sage : Disposable
	{
		internal class UpdateObject
		{
			internal INVENTORY_CODE_TYPE IType;

			internal TripDetailsList AccountInfo;


			static String ConvertAndFormatDate( String dT, out DateTime Time )
			{
				try
				{
					try
					{
						Time = DateTime.ParseExact( dT, @"yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture );
						return Time.ToString( "g" );
					}
					catch
					{
						Time = DateTime.ParseExact( dT, @"yyyy/MM/dd HH:mm", CultureInfo.InvariantCulture );
						return Time.ToString( "g" );
					}
				}
				catch
				{
					Time = DateTime.ParseExact( dT, @"yyyy/MM/dd", CultureInfo.InvariantCulture );
					return Time.ToString( "d" );
				}
			}

			internal UpdateObject( INVENTORY_CODE_TYPE iType, TripDetailsList accountInfo )
			{
				IType = iType;
				AccountInfo = accountInfo;
			}

			internal class ChargeDetails
			{
				internal string Description;
				internal double Value;

				internal ChargeDetails( String description, String value )
				{
					Description = description;

					if( !Double.TryParse( value, out Value ) )
						throw new ArgumentException( "Charge value must be numeric" );
				}
			}


			internal class TripPackageDetails
			{
				internal String ServiceLevel,
								PackageType,

								Quantity,
								Weight,
								Price,
								Extension;

				internal double dQuantity,
								dWeight,
								dPrice,
								dExtension;

				internal String TaxDesc;
				internal double TaxValue;

				internal String FuelSurchargeDesc;
				internal double FuelSurchargeValue;

				internal List<ChargeDetails> Charges = new List<ChargeDetails>();

				internal TripPackageDetails( String serviceLevel, String packageType,
											 String quantity, String weight, String price, String extension,
											 String tax1Desc, String tax1Value,
											 String tax2Desc, String tax2Value,
											 String fuelSurchargeDesc, String fuelSurchargeValue )
				{
					ServiceLevel = serviceLevel;
					PackageType = packageType;

					Quantity = quantity;
					Weight = weight;
					Price = price;
					Extension = extension;

					if( !Double.TryParse( quantity, out dQuantity ) )
						dQuantity = 0;

					if( !Double.TryParse( Weight, out dWeight ) )
						dWeight = 0;

					if( !Double.TryParse( price, out dPrice ) )
						throw new ArgumentException( "Trip price must be numeric" );

					if( !Double.TryParse( extension, out dExtension ) )
						throw new ArgumentException( "Trip extension must be numeric" );

					TaxDesc = ( tax1Desc.Trim() != "" ? tax1Desc : tax2Desc );

					double T1v;
					if( !Double.TryParse( tax1Value, out T1v ) )
						T1v = 0;

					double T2v;
					if( !Double.TryParse( tax2Value, out T2v ) )
						T2v = 0;

					TaxValue = T1v + T2v;

					FuelSurchargeDesc = fuelSurchargeDesc;
					if( !Double.TryParse( fuelSurchargeValue, out FuelSurchargeValue ) )
						FuelSurchargeValue = 0;
				}
			}

			internal class TripDetails
			{
				internal List<TripPackageDetails> Packages = new List<TripPackageDetails>();

				internal struct AddressDetails
				{
					internal DateTime Time;

					internal string FormattedDateTime,
									DateAndTime,
									Company,
									Reference,
									Address1,
									Address2,
									City,
									Region,
									PostCode,
									Country,
									Conatct,
									Telephone,
									Email,
									Notes;

					internal AddressDetails( String dateTime, String company, String reference,
											 String address1, String address2, String city, String region, String postCode, String country,
											 String contact, String telephone, String email, String notes )
					{
						var Provider = CultureInfo.InvariantCulture;

						dateTime = dateTime.Trim();
						FormattedDateTime = ConvertAndFormatDate( dateTime, out Time );

						DateAndTime = dateTime;
						Company = company.Trim();
						Reference = reference.Trim();
						Address1 = address1.Trim();
						Address2 = address2.Trim();
						City = city.Trim();
						Region = region.Trim();
						PostCode = postCode.Trim();
						Country = country.Trim();
						Conatct = contact.Trim();
						Telephone = telephone.Trim();
						Email = email.Trim();
						Notes = notes.Trim();
					}

				}

				internal String AccountId,		// Must be in sage 
								InvoiceNumber,
								TripId,
								TrailerDriver,
								DriverName,
								InvoiceDate,
								WayBill;

				internal AddressDetails Pickup,
										Delivered,
										Billing,
										Shipping;

				internal TripDetails( String accountId, String invoiceNumber, String invoiceDate,
										 String tripId, String driverName, String trailerDriver, 
										 String puDateTime, String puCompany, String puReference,
										 String puAddress1, String puAddress2, String puCity, String puRegion, String puPostCode, String puCountry,
										 String puContact, String puTelephone, String puEmail, String puNotes,
										 String delDateTime, String delCompany, String delReference,
										 String delAddress1, String delAddress2, String delCity, String delRegion, String delPostCode, String delCountry,
										 String delContact, String delTelephone, String delEmail, String delNotes,
										 String billCompany,
										 String billAddress1, String billAddress2, String billCity, String billRegion, String billPostCode, String billCountry,
										 String billContact, String billTelephone, String billEmail,
										 String shippingCompany,
										 String shippingAddress1, String shippingAddress2, String shippingCity, String shippingRegion, String shippingPostCode, String shippingCountry,
										 String shippingContact, String shippingTelephone, String shippingEmail,
										 String wayBill 
									)
				{
					AccountId = accountId.Trim();
					InvoiceNumber = invoiceNumber.Trim();
					InvoiceDate = invoiceDate.Trim();

					TripId = tripId.Trim();

					TrailerDriver = trailerDriver.Trim();
					DriverName = driverName.Trim();

					WayBill = wayBill;

					Pickup = new AddressDetails( puDateTime, puCompany, puReference,
												 puAddress1, puAddress2, puCity, puRegion, puPostCode, puCountry,
												 puContact, puTelephone, puEmail, puNotes );

					Delivered = new AddressDetails( delDateTime, delCompany, delReference,
													delAddress1, delAddress2, delCity, delRegion, delPostCode, delCountry,
													delContact, delTelephone, delEmail, delNotes );

					Billing = new AddressDetails( puDateTime, billCompany, puReference,
												  billAddress1, billAddress2, billCity, billRegion, billPostCode, billCountry,
												  billContact, billTelephone, billEmail, "" );

					Shipping = new AddressDetails( DateTime.Now.ToString( "yyyy-MM-dd HH:mm:ss" ), shippingCompany, "",
												   shippingAddress1, shippingAddress2, shippingCity, shippingRegion, shippingPostCode, shippingCountry,
												   shippingContact, shippingTelephone, shippingEmail, "" );
				}
			}

			internal class TripDetailsList : List<TripDetails>
			{
			}
		}
	}
}
