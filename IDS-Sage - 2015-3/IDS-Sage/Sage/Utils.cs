﻿

using System.Collections.Generic;
using IDS_Sage.Database;
using SimplySDK.Support;

namespace IDS_Sage.Import
{
	internal partial class Ids_Sage
	{
		internal class PackedCustomerXref : Dictionary<string, string>
		{
			internal void Add( string value )
			{
				var Key = DbCustomers.PackCode( value );
				if( !ContainsKey( Key ) )
					base.Add( DbCustomers.PackCode( value ), value );
			}
		}

		internal static PackedCustomerXref GetAllCustomersCodesPacked()
		{
			var RetVal = new PackedCustomerXref();
			var Util = new SDKDatabaseUtility();

			var Count = Util.RunSelectQuery( "SELECT sName FROM tCustomr" );
			for( var index = 0; index < Count; index++ )
				RetVal.Add( Util.GetStringFromLastSelectQuery( index, 0 ) );
			
			return RetVal;
		}
	}
}


