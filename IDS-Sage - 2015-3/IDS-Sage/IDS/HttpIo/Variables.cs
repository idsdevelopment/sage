
using System.Net;
using System.Net.Cache;
using System.Threading.Tasks;

namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo
	{
		private const int INITIAL_TIMEOUT = 60 * 60 * 1000;

		static readonly HttpRequestCachePolicy NoCachePolicy = new HttpRequestCachePolicy( HttpRequestCacheLevel.NoCacheNoStore );

		readonly RequestQueue WebIoQueue = new RequestQueue();
		bool Closing;

		private static readonly object CookieLock = new object();

		private static CookieContainer GlobalCookies = new CookieContainer();
	}
}
