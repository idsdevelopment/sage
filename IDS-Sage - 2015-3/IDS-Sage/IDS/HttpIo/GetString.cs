﻿namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		public static string GetString( string url )
		{
			var PageContent = "";
			var Ok = false;

			WebIo.Get( new RequestEntry( url, ( RequestEntry R ) =>
			{
				PageContent = R.Content;
				Ok = true;
			},
			( RequestEntry R ) =>
			{
			},
			true ) );

			return( Ok ? PageContent : null );
		}
	}
}