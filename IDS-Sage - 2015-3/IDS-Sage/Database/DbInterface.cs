﻿namespace IDS_Sage.Database
{
	interface DbInterface
	{
		void Open( string dbPath );
		void Close();
		Customer GetCustomerByAccountId( string accountId );
		void Add( Customer custRec );
		void Remove( string accountId );
		void EmptyTable();
	}
}
