﻿using System;

namespace IDS_Sage
{
	internal static class Globals
	{
		internal const string ERROR_EXTENSION = ".error.log",
							  CSV_EXTENSION = ".csv";
	}
}
