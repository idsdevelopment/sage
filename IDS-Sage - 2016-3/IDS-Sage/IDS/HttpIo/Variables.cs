
using System.Net;
using System.Net.Cache;
using System.Threading.Tasks;

namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo
	{
		public const int INITIAL_TIMEOUT = 90000;		// 90 secs

		private static readonly HttpRequestCachePolicy NoCachePolicy = new HttpRequestCachePolicy( HttpRequestCacheLevel.NoCacheNoStore );

		private readonly RequestQueue WebIoQueue = new RequestQueue();
		private Task PostTask;
		private bool Closing;

		public static object CookieLock = new object();

		public static CookieContainer GlobalCookies = new CookieContainer();
	}
}
