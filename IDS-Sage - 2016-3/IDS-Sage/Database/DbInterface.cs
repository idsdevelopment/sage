﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IDS_Sage.Database
{
	internal interface DbInterface
	{
		void Open( string dbPath );
		void Close();
		Customer GetCustomerByAccountId( string accountId );
		void Add( Customer custRec );
		void Remove( string accountId );
		void EmptyTable();
	}
}
