﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Windows.Forms;
using System.Xml;

namespace IDS_Sage
{
	internal static class  Utils
	{
		internal static string AddPathSeparator( string FullPath )
		{
			return( FullPath.TrimEnd().TrimEnd( Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar ) + Path.DirectorySeparatorChar );
		}


		internal static bool ChangeConnectionString( string AppName, string dbName, string databasePathName, string providerName = "" )
		{
			var retVal = false;
			try
			{
				var FILE_NAME = string.Concat( Application.StartupPath, "\\", AppName.Trim(), ".Config" ); //the application configuration file name
				var reader = new XmlTextReader( FILE_NAME );
				var doc = new XmlDocument();
				doc.Load( reader );
				reader.Close();
				var nodeRoute = string.Concat( "connectionStrings/add" );

				XmlNode cnnStr = null;
				var root = doc.DocumentElement;
				var Settings = root.SelectNodes( nodeRoute );

				for( var i = 0; i < Settings.Count; i++ )
				{
					cnnStr = Settings[ i ];
					if( cnnStr.Attributes[ "name" ].Value.Equals( dbName ) )
						break;
					cnnStr = null;
				}

				cnnStr.Attributes[ "connectionString" ].Value = databasePathName;
				if( providerName != "" )
						cnnStr.Attributes[ "providerName" ].Value = providerName;
				doc.Save( FILE_NAME );
				retVal = true;
			}
			catch
			{
				retVal = false;
			}
			return retVal;
		}
	}
}
