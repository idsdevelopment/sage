﻿using System;
using System.Globalization;
using System.IO;
using IDS_Sage.Database;
using Simply.Domain.Utility;
using SimplySDK;
using SimplySDK.Support;
using System.Threading;


namespace IDS_Sage.Import
{
	internal partial class Ids_Sage : Disposable
	{
		internal enum INVENTORY_CODE_TYPE { BY_SERVICE_PACKAGE, BY_PACKAGE_SERVICE, BY_DRIVER };

		INVENTORY_CODE_TYPE InventoryCodeType;

		bool ByDriver;

		String DbPath, UserName, Password;
		int SalesLedger,
			FuelSurchargeLedger;

		short AccountNumumberLength;

		internal bool IgnoreCache;

		public class AlwaysYesAlert : SDKAlert
		{
			public override AlertResult AskAlert( SimplyMessage message )
			{
				return AlertResult.YES;
			}
		}

		protected override void OnDispose( bool systemDisposing )
		{
			SDKInstanceManager.Instance.CloseDatabase();
		}

		protected Ids_Sage( String dbPath, String userName, String password, String salesLedger, String fuelSurchargeLedger, INVENTORY_CODE_TYPE inventoryCodeType, bool ignoreCache )
		{
			DbPath = dbPath.TrimEnd();
			UserName = userName.Trim();
			Password = password;
			InventoryCodeType = inventoryCodeType;

			IgnoreCache = ignoreCache;

			ByDriver = ( InventoryCodeType == INVENTORY_CODE_TYPE.BY_DRIVER );

			if( !Int32.TryParse( salesLedger.Trim(), out SalesLedger ) )
				throw new ArgumentException( "Sales ledger must be numeric." );

			if( !Int32.TryParse( fuelSurchargeLedger.Trim(), out FuelSurchargeLedger ) )
				throw new ArgumentException( "Fuel surcharge ledger must be numeric." );

			if( !SDKInstanceManager.Instance.OpenDatabase( DbPath, UserName.Trim(), Password, true, "Ids-Sage-Import", "IDS", 1 ) )
				throw new Exception( "Cannot open Sage database." );

			AccountNumumberLength = (short)( new SDKDatabaseUtility() ).RunScalerQuery( "SELECT nActNumLen FROM tCompOth" );

			SDKInstanceManager.Instance.SetAlertImplementation( new AlwaysYesAlert() );
		}

		private int MakeAccountNumber( int number, short maxLen )
		{
			string zeros = string.Empty;
			string num_str = ( number <= 0 ) ? "1" : num_str = Convert.ToString( number );

			for( int i = num_str.Length; i < Math.Min( (short)8, maxLen ); i++ )
				zeros = zeros + "0";

			return Convert.ToInt32( num_str + zeros );
		}

		private int MakeAccountNumber( int number )
		{
			return MakeAccountNumber( number, AccountNumumberLength );
		}

		private static string TrimIt( string str, int maxLen )
		{
			str = str.Trim();
			if( str.Length > maxLen )
				str = str.Substring( 0, maxLen );
			return str;
		}

		internal void UpdateCustomer( string oldCustKey, Customer cust )
		{
			var CustLedger = SDKInstanceManager.Instance.OpenCustomerLedger();
			try
			{
				if( !CustLedger.LoadByName( oldCustKey ) )
					CustLedger.InitializeNew();

				CustLedger.Name = cust.CustomerName;

				if( cust.AddressLine1 == "" )		// No Suite
				{
					cust.AddressLine1 = cust.AddressLine2;
					cust.AddressLine2 = "";
				}

				CustLedger.Street1 = TrimIt( cust.AddressLine1, 50 );
				CustLedger.Street2 = TrimIt( cust.AddressLine2, 50 );
				CustLedger.City = TrimIt( cust.City, 35 );
				CustLedger.Province = TrimIt( cust.Region, 20 );
				CustLedger.PostalCode = TrimIt( cust.PostCode, 9 );
				CustLedger.Country = TrimIt( cust.Country, 30 );

				CustLedger.Save();
			}
			catch
			{
				CustLedger.Undo();
				throw;
			}
			finally
			{
				SDKInstanceManager.Instance.CloseCustomerLedger();
			}
		}

		private string BuildInventoryKey( UpdateObject.TripDetails trip, UpdateObject.TripPackageDetails package )
		{
			switch( InventoryCodeType )
			{
			case INVENTORY_CODE_TYPE.BY_DRIVER:
				return trip.DriverName.Trim();
			case INVENTORY_CODE_TYPE.BY_PACKAGE_SERVICE:
				return ( package.PackageType + " - " + package.ServiceLevel ).Trim();
			default:
				return ( package.ServiceLevel + " - " + package.PackageType ).Trim();
			}
		}

		protected void Update( UpdateObject updateData )
		{
			var CustLedger = SDKInstanceManager.Instance.OpenCustomerLedger();
			try
			{
				var AccountInfo = updateData.AccountInfo;
				var PrimaryAccount = AccountInfo[ 0 ];

					// Billing Company Assumed To Be First In List
				var Billing = PrimaryAccount.Billing;
				String CustCode = Billing.Company;

				if( !CustLedger.LoadByName( CustCode ) )
				{
					if( !ByDriver )
					{
						CustLedger.InitializeNew();
						CustLedger.Name = CustCode;

						if( Billing.Address1 == "" )		// No Suite
						{
							Billing.Address1 = Billing.Address2;
							Billing.Address2 = "";
						}

						CustLedger.Street1 = TrimIt( Billing.Address1, 50 );
						CustLedger.Street2 = TrimIt( Billing.Address2, 50 );
						CustLedger.City = TrimIt( Billing.City, 35 );
						CustLedger.Province = TrimIt( Billing.Region, 20 );
						CustLedger.PostalCode = TrimIt( Billing.PostCode, 9 );
						CustLedger.Country = TrimIt( Billing.Country, 30 );

						CustLedger.Save();
					}
					else
						throw new Exception( "Missing Sage Customer Code : " + CustCode );
				}

				var InvLedger = SDKInstanceManager.Instance.OpenInventoryLedger();
				try
				{
					Action<String, bool> CheckInventory = ( String itemCode, bool isFuelSurcharge ) =>
					{
						if( !InvLedger.LoadByPartCode( itemCode ) )
						{
							if( !ByDriver || isFuelSurcharge )
							{
								InvLedger.InitializeNew();
								InvLedger.Number = itemCode;
								InvLedger.Name = "";
								InvLedger.StockingUnit = "Each";
								InvLedger.IsServiceType = true;
								InvLedger.RevenueAccount = MakeAccountNumber( isFuelSurcharge ? FuelSurchargeLedger : SalesLedger ).ToString();
								InvLedger.Save();
							}
							else
								throw new Exception( "Missing Sage Driver Code : " + itemCode );
						}
						else if( InvLedger.RevenueAccount.Trim() == "" )
						{
							InvLedger.RevenueAccount = MakeAccountNumber( SalesLedger ).ToString();
							InvLedger.Save();
						}
					};


					// Invoice
					var SalesJournal =  SDKInstanceManager.Instance.OpenSalesJournal();
					try
					{
						SalesJournal.SelectTransType( 0 );				// invoice
						try
						{
							SalesJournal.SelectAPARLedger( CustCode );
						}
						catch( Exception E )
						{
							if( E.Message.IndexOf( "This customer often pays late." ) < 0 )
								throw;
						}

						int Line = 1;
						var Ref = PrimaryAccount.InvoiceNumber.Trim();

						if( Ref != "" )
							SalesJournal.InvoiceNumber = Ref;

						SalesJournal.SetJournalDate( PrimaryAccount.InvoiceDate );

						var ShipTo = PrimaryAccount.Shipping;

						var ShipLineNumber = 1;

						var Adr = ShipTo.Company.Trim();
						if( Adr != "" )
							SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

						Adr = ShipTo.Address1.Trim();
						if( Adr != "" )
							SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

						Adr = ShipTo.Address2.Trim();
						if( Adr != "" )
							SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );

						SalesJournal.SetShipToAddressLine( TrimIt( ShipTo.City, 35 ), ShipLineNumber++ );

						Adr = ShipTo.Region + "  " + ShipTo.PostCode;
						SalesJournal.SetShipToAddressLine( TrimIt( Adr, 50 ), ShipLineNumber++ );
						SalesJournal.SetShipToAddressLine( TrimIt( ShipTo.Country, 30 ), ShipLineNumber++ );
							
						Action<String, UpdateObject.TripDetails.AddressDetails> BuildAddress = ( String PreFix, UpdateObject.TripDetails.AddressDetails Addr ) =>
						{
							SalesJournal.SetDescription( "", Line++ );

							String Description = PreFix + " Address: " + Addr.Company;
							SalesJournal.SetDescription( Description, Line++ );

							var Temp = Addr.Address1.Trim();
							if( Temp != "" )
								SalesJournal.SetDescription( Temp, Line++ );

							Temp =  Addr.Address2.Trim();
							if( Temp != "" )
								SalesJournal.SetDescription( Temp, Line++ );

							Description = Addr.City + "  " + Addr.Region + "  " + Addr.PostCode;
							SalesJournal.SetDescription( Description, Line++ );

							Temp = Addr.Country.Trim();
							if( Temp != "" )
								SalesJournal.SetDescription( Temp, Line++ );

							Description = "";
							Temp = Addr.Conatct.Trim();
							if( Temp != "" )
								Description = Temp;

							Temp = Addr.Telephone.Trim();
							if( Temp != "" )
								Description += "  " + Temp;

							if( Description != "" )
								SalesJournal.SetDescription( Description, Line++ );
						};

						double TotalTax = 0;
						string TaxAdjTaxId = "";

						foreach( var Trip in AccountInfo )
						{
							SalesJournal.SetDescription( "Trip ID: " + Trip.TripId, Line++ );
							SalesJournal.SetDescription( "Waybill: " + Trip.WayBill, Line++ );

							if( !ByDriver && Trip.DriverName != "" )
							{
								string Details = "Driver: " + Trip.DriverName;
								SalesJournal.SetDescription( Details, Line++ );

								Details = "  Pickup Time : " + Trip.Pickup.Time.ToString( CultureInfo.CurrentCulture.DateTimeFormat );
								SalesJournal.SetDescription( Details, Line++ );

								Details = "  Delivery Time : " + Trip.Delivered.Time.ToString( CultureInfo.CurrentCulture.DateTimeFormat );
								SalesJournal.SetDescription( Details, Line++ );

								BuildAddress( "Pickup", Trip.Pickup );
								BuildAddress( "Delivery", Trip.Delivered );
							}

							foreach( var Package in Trip.Packages )
							{
								TotalTax += Package.TaxValue;

								var ICode = BuildInventoryKey( Trip, Package );

								CheckInventory( ICode, false );

								SalesJournal.SetItemNumber( ICode, Line );
								SalesJournal.SetDescription( InvLedger.Name, Line );
								SalesJournal.SetQuantity( ByDriver ? 1 : Package.dQuantity, Line );
								SalesJournal.SetUnit( InvLedger.StockingUnit, Line );

								string TripTaxId;
								if( Package.TaxValue == 0 )
									TripTaxId = "E";
								else
									TripTaxId = Package.TaxDesc;

								try
								{
									SalesJournal.SetTaxCodeString( TripTaxId, Line );
								}
								catch
								{
									try
									{
										TripTaxId = TripTaxId.Substring( 0, 1 );
										SalesJournal.SetTaxCodeString( TripTaxId, Line );
									}
									catch
									{
										throw new Exception( "Unknown Tax Id: '" + TripTaxId + "'" );
									}
								}


								if( TaxAdjTaxId == "" )
									TaxAdjTaxId = TripTaxId;

								double ChargeTotal = 0;
								if( ByDriver )			// Sum Charges
								{
									foreach( var Charge in Package.Charges )
										ChargeTotal += Charge.Value;
								}

								SalesJournal.SetPrice( Package.dPrice, Line );

								var TotalAmount = Package.dExtension + ChargeTotal;
								SalesJournal.SetLineAmount( TotalAmount, Line++ );

								if( !ByDriver )
								{
									foreach( var Charge in Package.Charges )
									{
										var Value = Charge.Value;
										if( Value != 0 )
										{
											var Desc =  "CHARGE_" + Charge.Description.Trim();
											CheckInventory( Desc, false );
											SalesJournal.SetItemNumber( Desc, Line );
											SalesJournal.SetDescription( InvLedger.Name, Line );
											SalesJournal.SetUnit( InvLedger.StockingUnit, Line );
											SalesJournal.SetQuantity( 1, Line );
											SalesJournal.SetLineAmount( Value, Line );
											SalesJournal.SetTaxCodeString( TripTaxId, Line++ );
										}
									}
								}
/*
								else
								{

//#if DEBUG
									// Update Projects
									var Pledger =  SDKInstanceManager.Instance.OpenProjectLedger();
									try
									{
											// Get Project
										if( !Pledger.LoadByName( ICode ) )
										{
											Pledger.InitializeNew();
											Pledger.Name = ICode;
											Pledger.NameAlt = ICode;
											Pledger.StartDate = DateTime.Now.AddYears( -1 );
											Pledger.Save();
										}

											// Allocate Projects
										ProjectAllocation ProjAlloc = SalesJournal.AllocateLine( Line++ );
										ProjAlloc.SetProject( ICode, 1 );
										ProjAlloc.SetAmount( TotalAmount, 1 );
										ProjAlloc.Save();
									}
									catch
									{
										Pledger.Undo();
										throw;
									}
//#endif
								}
*/

								if( Package.FuelSurchargeValue != 0 )
								{
									var Desc =  "CHARGE_" + Package.FuelSurchargeDesc;
									CheckInventory( Desc, true );
									SalesJournal.SetItemNumber( Desc, Line );
									SalesJournal.SetDescription( InvLedger.Name, Line );
									SalesJournal.SetUnit( InvLedger.StockingUnit, Line );
									SalesJournal.SetQuantity( 1, Line );
									SalesJournal.SetLineAmount( Package.FuelSurchargeValue, Line );
									SalesJournal.SetTaxCodeString( TripTaxId, Line++ );
								}
							}
						}

						var SageTotalTax = SalesJournal.GetTaxTotalAmount();

						TotalTax = Math.Round( TotalTax, 2, MidpointRounding.AwayFromZero );

						if( SageTotalTax != TotalTax )
						{
							SalesJournal.SetFreightTaxCode( "E" );
							var TaxDiff = Math.Round( TotalTax - SageTotalTax, 2, MidpointRounding.AwayFromZero );

							try
							{
								SalesJournal.SetFreightTax1Amount( TaxDiff );
							}
							catch			// Can't set customer record has tax rated
							{
								SalesJournal.SetFreightAmount( TaxDiff );
							}
						}

						try
						{
							if( !SalesJournal.Post() )
								throw new IOException( "Cannot post sale." );
						}
						catch( Exception E )
						{
							if( E.Message.IndexOf( "processed successfully" ) < 0
								&& E.Message.IndexOf( "Fast Posting" ) < 0 
								&& E.Message.IndexOf( "This customer often pays late." ) < 0 )
									throw;
						}
					}
					catch
					{
						SalesJournal.Undo();
						throw;
					}
					finally
					{
						SDKInstanceManager.Instance.CloseSalesJournal();
					}
				}
				catch
				{
					InvLedger.Undo();
					throw;
				}
				finally
				{
					SDKInstanceManager.Instance.CloseInventoryLedger();
				}
			}
			catch
			{
				CustLedger.Undo();
				throw;
			}
			finally
			{
				SDKInstanceManager.Instance.CloseCustomerLedger();
			}
		}
	}
}
