using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Threading;
using System.Threading.Tasks;


namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		private static HttpIo _WebIo;

		public static HttpIo WebIo		// Had Problem with static initialisation
		{
			get
			{
				if( _WebIo == null )
					_WebIo = new HttpIo();

				return _WebIo;
			}
		}

		public HttpIo()
		{
			InitCookie();

			PostTask = Task.Factory.StartNew( () =>
			{
				while( !Closing )
				{
					if( WebIoQueue.Count > 0 )
					{
						RequestEntry R = WebIoQueue.NextEntry;
						try
						{
							var Request = (HttpWebRequest)WebRequest.Create( R.Url );
							Request.KeepAlive = true;
							Request.CachePolicy = NoCachePolicy;
							Request.CookieContainer = R.Cookies;

							if( R.Operation == RequestEntry.REQUEST_TYPE.POST )
							{
								Request.Method = "POST";

								string Boundary = "--------B0UnDarY---------" + DateTime.Now.Ticks.ToString( "x" );

								Request.ContentType = "multipart/form-data; boundary=" + Boundary;
								Request.Credentials = System.Net.CredentialCache.DefaultCredentials;

								using( Stream MemStream = new System.IO.MemoryStream() )
								{
									try
									{
										byte[] BoundaryBytes = System.Text.Encoding.ASCII.GetBytes( "\r\n--" + Boundary + "\r\n" );

										string FormdataTemplate = "\r\n--" + Boundary + "\r\nContent-Disposition: form-data; name=\"{0}\";\r\n\r\n{1}";

										foreach( var Kv in R )
										{
											if( !Kv.Value.IsFile )
											{
												string FormItem = string.Format( FormdataTemplate, Parameters.KeyAsString( Kv ), Parameters.ValueAsString( Kv ) );
												byte[] FormItemBytes = System.Text.Encoding.UTF8.GetBytes( FormItem );
												MemStream.Write( FormItemBytes, 0, FormItemBytes.Length );
											}
										}

										MemStream.Write( BoundaryBytes, 0, BoundaryBytes.Length );

										string FileTemplate = "Content-Disposition: form-data;name=\"{0}\";filename=\"{1}\"\r\nContent-Type: application/octet-stream\r\n\r\n";
										int I = 0;

										foreach( var Kv in R )
										{
											if( Kv.Value.IsFile )
											{
												string Header = string.Format( FileTemplate, "File" + I++, Kv.Value.FileName );
												byte[] HeaderBytes = System.Text.Encoding.UTF8.GetBytes( Header );
												MemStream.Write( HeaderBytes, 0, HeaderBytes.Length );

												byte[] Data = Kv.Value.AsBytes;
												MemStream.Write( Data, 0, Data.Length );

												MemStream.Write( BoundaryBytes, 0, BoundaryBytes.Length );
											}
										}

										Stream RequestStream = Request.GetRequestStream();
										try
										{
											byte[] TempBuffer = new byte[ MemStream.Length ];
											MemStream.Position = 0;

											MemStream.Read( TempBuffer, 0, TempBuffer.Length );
											MemStream.Close();

											RequestStream.Write( TempBuffer, 0, TempBuffer.Length );
										}
										finally
										{
											RequestStream.Close();
										}
									}
									finally
									{
										MemStream.Close();
									}
								}
							}
							else			// Url has GET values
								Request.Method = "GET";

							Request.Timeout = R.Timeout;

							string Content = null;
							byte[] BinaryContent = null;

							using( HttpWebResponse Response = (HttpWebResponse)Request.GetResponse() )
							{
								if( Response.StatusCode == HttpStatusCode.OK )
								{
									try
									{
										using( var ResponseStream = Response.GetResponseStream() )
										{
											using( MemoryStream Stream = new MemoryStream() )
											{
												int Count = 0;
												byte[] buffer = new byte[ 4096 ];

												do
												{
													Count = ResponseStream.Read( buffer, 0, buffer.Length );
													Stream.Write( buffer, 0, Count );

												} while( Count != 0 );

												BinaryContent = Stream.ToArray();
												Content = System.Text.ASCIIEncoding.ASCII.GetString( BinaryContent );
											}
										}
									}
									finally
									{
										Response.Close();
									}
								}

								lock( CookieLock )
								{
									if( GlobalCookies == null )
										InitCookie();

									try
									{
										GlobalCookies.Add( Response.Cookies );
									}
									catch
									{
									}

									R.Cookies = GlobalCookies;
								}
							}

							if( BinaryContent != null )
							{
								R.Error = "Ok";
								R.Content = Content;
								R.BinaryContent = BinaryContent;

								if( R.OnSuccess != null )
									R.OnSuccess( R );
							}
							else if( R.OnFail != null )
							{
								R.Content = R.Error = "No Response";
								R.OnFail( R );
							}
						}
						catch( Exception e )
						{
#if DEBUG
							Debug.Print ( "Http Response Exception", e.Message );
#endif
							R.Content = R.Error = e.Message;
							R.OnFail( R );
						}
						finally
						{
							R.Waiting = false;
						}
					}
					else
						Thread.Sleep( 100 );
				}
				Closing = false;
			} );
		}

		protected override void OnDispose( bool systemDisposing )
		{
			Closing = true;
			while( Closing )
				Thread.Sleep( 100 );

			lock( CookieLock )
				GlobalCookies = null;
		}

		public void Post( RequestEntry entry )
		{
			entry.Operation = RequestEntry.REQUEST_TYPE.POST;
			WebIoQueue.Add( entry );
			while( entry.Waiting )
				Thread.Sleep( 100 );
		}

		public void Get( RequestEntry entry )
		{
			entry.Operation = RequestEntry.REQUEST_TYPE.GET;
			WebIoQueue.Add( entry );
			while( entry.Waiting )
				Thread.Sleep( 100 );
		}
	}
}

