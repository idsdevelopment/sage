﻿using IDS_Sage;

namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo : Disposable
	{
		public static string GetString( string url )
		{
			string PageContent = "";
			bool Ok = false;

			WebIo.Get( new HttpIo.RequestEntry( url, ( HttpIo.RequestEntry R ) =>
			{
				PageContent = R.Content;
				Ok = true;
			},
			( HttpIo.RequestEntry R ) =>
			{
			},
			true ) );

			return( Ok ? PageContent : null );
		}
	}
}