
using System.Net;
using System.Net.Cache;
using System.Threading.Tasks;

namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo
	{
		public const int INITIAL_TIMEOUT = 90000;		// 90 secs

		static HttpRequestCachePolicy NoCachePolicy = new HttpRequestCachePolicy( HttpRequestCacheLevel.NoCacheNoStore );

		HttpIo.RequestQueue WebIoQueue = new HttpIo.RequestQueue();
		Task PostTask;
		bool Closing;

		public static object CookieLock = new object();

		public static CookieContainer GlobalCookies = new CookieContainer();
	}
}
