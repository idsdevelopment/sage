﻿using System;
using System.IO;
using IDS_Sage.Import;

namespace IDS_Sage.IDS
{
	internal class IdsReMap
	{
		private static String[] Colons =  new String[] { "::" };

		enum TAX_TYPE { NONE, TAX_1, TAX_2, FSG };

		internal enum FIELDS
		{
			IDS_INVOICE_NUMBER,
			IDS_DRIVER_ASSIGN_TIME,
			IDS_READY_TIME,
			IDS_INVOICE_DATE_TIME,
			IDS_SHIPMENT_ID,
			IDS_ACCOUNT_ID,
			IDS_CALL_TIME,

			IDS_PICKUP_COMPANY_NAME,
			IDS_PICKUP_SUITE,
			IDS_PICKUP_STREET,
			IDS_PICKUP_CITY,
			IDS_PICKUP_REGION,
			IDS_PICKUP_COUNTRY,
			IDS_PICKUP_POSTAL_CODE,
			IDS_PICKUP_ZONE,

			IDS_DELIVERY_COMPANY_NAME,
			IDS_DELIVERY_SUITE,
			IDS_DELIVERY_STREET,
			IDS_DELIVERY_CITY,
			IDS_DELIVERY_REGION,
			IDS_DELIVERY_COUNTRY,
			IDS_DELIVERY_POSTAL_CODE,
			IDS_DELIVERY_ZONE,

			IDS_BILLING_COMPANY_NAME,
			IDS_BILLING_SUITE,
			IDS_BILLING_STREET,
			IDS_BILLING_CITY,
			IDS_BILLING_REGION,
			IDS_BILLING_COUNTRY,
			IDS_BILLING_POSTAL_CODE,
			IDS_BILLING_PHONE,

			IDS_WEIGHT,
			IDS_PIECES,

			IDS_SERVICE_LEVEL,
			IDS_PACKAGE_TYPE,
			IDS_DELIVERY_AMOUNT,
			IDS_TOTAL_TAX,
			IDS_INVOICE_TOTAL_INCLUDING_TAX,

			IDS_DELIVERY_DRIVER_NAME,
			IDS_CALL_TAKER_ID,

			IDS_SHIPPING_SUITE,
			IDS_SHIPPING_STREET,
			IDS_SHIPPING_CITY,
			IDS_SHIPPING_REGION,
			IDS_SHIPPING_COUNTRY,
			IDS_SHIPPING_POSTAL_CODE,

			IDS_PICKUP_TIME,
			IDS_DELEVERY_TIME,

			IDS_WAYBILL,

			IDS_BEGIN_CHARGES
		}

		// 20060921_230520  --> 2006-06-021 23:05:20
		private static string FixDate( String FunnyDate )
		{
			var Parts = FunnyDate.Trim().Split( '_' );
			if( Parts.Length == 2 )
			{
				string Date = Parts[ 0 ].Substring( 0, 4 ) + "-" + Parts[ 0 ].Substring( 4, 2 ) + "-" + Parts[ 0 ].Substring( 6, 2 ),
						Time = Parts[ 1 ].Substring( 0, 2 ) + ":" + Parts[ 1 ].Substring( 2, 2 ) + ":" + Parts[ 1 ].Substring( 4, 2 );

				return Date + " " + Time;
			}
			return "00-00-00 00:00:00";
		}

		internal static Stream ReMapCsv( Stream Csv, bool debugMode, string errorPath )
		{
			errorPath = Utils.AddPathSeparator( errorPath );

			Csv.Position = 0;

			CsvReader Reader = new CsvReader( Csv );

			Action<String> DoDebug = ( fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						using( FileStream Stream = new FileStream( errorPath + fileName + Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
						{
							Reader.WriteToStream( Stream );
						}
					}
					catch
					{
					}
				}
			};

			Action<String, String> DoDebugText = ( csvText, fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						using( FileStream Stream = new FileStream( errorPath + fileName + Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
						{
							var Bytes = System.Text.Encoding.UTF8.GetBytes( csvText );
							Stream.Write( Bytes, 0, Bytes.Length );
						}
					}
					catch
					{
					}
				}
			};


			 DoDebug( "Afer Ids Import (Before Sort)" );

			// Sort By Invoice Number / Call Time
			Reader.Sort( ( CsvReader.Columns a, CsvReader.Columns b ) =>
			{
				var RetVal = String.CompareOrdinal( a[ (int)FIELDS.IDS_ACCOUNT_ID ], b[ (int)FIELDS.IDS_ACCOUNT_ID ] );
				if( RetVal == 0 )
				{
					RetVal = String.CompareOrdinal( a[ (int)FIELDS.IDS_INVOICE_NUMBER ], b[ (int)FIELDS.IDS_INVOICE_NUMBER ] );
					if( RetVal == 0 )
						RetVal = String.CompareOrdinal( a[ (int)FIELDS.IDS_SHIPMENT_ID ], b[ (int)FIELDS.IDS_SHIPMENT_ID ] );
				}
				return RetVal;
			} );

			DoDebug( "Afer Ids Import (After Sort)" );

				// Adjust For Charges
			foreach( var ImportLine in Reader )
			{
				double ChargeAdj = 0;

				for( int I = (int)FIELDS.IDS_BEGIN_CHARGES, C = ImportLine.Count; I < C; )
				{
					var Charge = ImportLine[ I++ ].Trim();
					if( Charge != "" )
					{
						var Parts = Charge.Split( Colons, StringSplitOptions.RemoveEmptyEntries );
						if( Parts.Length == 2 )
						{
							var Val = Parts[ 1 ].Trim();
							if( Val != "" )
							{
								double Temp;
								if( !Double.TryParse( Parts[ 1 ], out Temp ) )
									Temp = 0;

								ChargeAdj += Temp;
							}
						}
					}
					else
						break;
				}

				double Amount;

				if( Double.TryParse( ImportLine[ (int)FIELDS.IDS_INVOICE_TOTAL_INCLUDING_TAX ], out Amount ) )
					ImportLine[ (int)FIELDS.IDS_INVOICE_TOTAL_INCLUDING_TAX ] =  ( Amount - ChargeAdj ).ToString( "0.##" );
			}


			DoDebug( "Afer Adjust For Charges" );

			string	CsvFile = "",
					Line = "",
					LastInv = "",
					LastAccountId = "",
					LastTripId = "";

			Action<string> AddField = ( string Field ) =>
			{
				if( Line != "" )
					Line += ",";

				Line += '"' + Field + '"';
			};

			Action EndOfLine = () =>
			{
				CsvFile += ( Line + "\r\n" );
				Line = "";
			};

			Action EndOfInvoice = () =>
			{
				if( LastAccountId != "" )
				{
					// Build Account Line
					AddField( SageImport.HEADER_COL.END_OF_INVOICE );	// Line Type Id
					AddField( LastAccountId );
					AddField( LastInv );
					EndOfLine();
				}
			};

			foreach( var ImportLine in Reader )
			{
				var AccountId = ImportLine[ (int)FIELDS.IDS_ACCOUNT_ID ];

						//Remove trailing period BUG at IDS
				var L = AccountId.Length;
				if( L-- <= 0 )
					continue;

				if( AccountId[ L ] == '.' )
				{
					if( L == 0 )
						continue;

					AccountId = AccountId.Substring( 0, L );
				}

				var Inv = ImportLine[ (int)FIELDS.IDS_INVOICE_NUMBER ];
				var TripId = ImportLine[ (int)FIELDS.IDS_SHIPMENT_ID ];
				bool Eoi = ( LastAccountId != AccountId || LastInv != Inv );

				if( Eoi || TripId != LastTripId )
				{
					if( Eoi )
						EndOfInvoice();

					LastAccountId = AccountId;
					LastInv = Inv;
					LastTripId = TripId;


					// Build Account Line
					AddField( SageImport.HEADER_COL.ACCOUNT );	// Line Type Id

					AddField( AccountId );												// ACCOUNT_ID
					AddField( Inv );													// INVOICE_NUMBER
					AddField( TripId );													// TRIP_ID

					var InvDate = FixDate( ImportLine[ (int)FIELDS.IDS_INVOICE_DATE_TIME ] );
					AddField( InvDate );												// INVOICE_DATE

					AddField( FixDate( ImportLine[ (int)FIELDS.IDS_CALL_TIME ] ) );		// CALL_TIME		!Correct Format
					AddField( InvDate );												// DELIVERY_DATE_TIME
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_TIME ] );				// PICKUP TIME
					AddField( ImportLine[ (int)FIELDS.IDS_DELEVERY_TIME ] );			// DELIVERY TIME

					AddField( "" );														// PICKUP_REFERENCE
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_COMPANY_NAME ] );		// PICKUP_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_SUITE ] );				// PICKUP_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_STREET ] );			// PICKUP_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_CITY ] );				// PICKUP_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_REGION ] );			// PICKUP_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_COUNTRY ] );			// PICKUP_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_POSTAL_CODE ] );		// PICKUP_POST_CODE
					AddField( ImportLine[ (int)FIELDS.IDS_PICKUP_ZONE ] );				// PICKUP_ZONE
					AddField( "" );														// PICKUP_CONTACT
					AddField( "" );														// PICKUP_TELEPHONE
					AddField( "" );														// PICKUP_EMAIL
					AddField( "" );														// PICKUP_NOTES

					AddField( "" );														// DELIVERY_REFERENCE
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_COMPANY_NAME ] );	// DELIVERY_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_SUITE ] );			// DELIVERY_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_STREET ] );			// DELIVERY_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_CITY ] );			// DELIVERY_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_REGION ] );			// DELIVERY_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_COUNTRY ] );			// DELIVERY_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_POSTAL_CODE ] );		// DELIVERY_POST_CODE
					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_ZONE ] );			// DELIVERY_ZONE
					AddField( "" );														// DELIVERY_CONTACT
					AddField( "" );														// DELIVERY_TELEPHONE
					AddField( "" );														// DELIVERY_EMAIL
					AddField( "" );														// DELIVERY_NOTES

					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COMPANY_NAME ] );		// BILLING_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_SUITE ] );			// BILLING_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_STREET ] );			// BILLING_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_CITY ] );				// BILLING_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_REGION ] );			// BILLING_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COUNTRY ] );			// BILLING_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_POSTAL_CODE ] );		// BILLING_POST_CODE
					AddField( "" );														// BILLING_CONTACT
					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_PHONE ] );			// BILLING_TELEPHONE
					AddField( "" );														// BILLING_EMAIL

					AddField( ImportLine[ (int)FIELDS.IDS_BILLING_COMPANY_NAME ] );		// SHIPPING_COMPANY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_SUITE ] );			// SHIPPING_ADDRESS_1
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_STREET ] );			// SHIPPING_ADDRESS_2
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_CITY ] );			// SHIPPING_CITY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_REGION ] );			// SHIPPING_REGION
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_COUNTRY ] );			// SHIPPING_COUNTRY
					AddField( ImportLine[ (int)FIELDS.IDS_SHIPPING_POSTAL_CODE ] );		// SHIPPING_POST_CODE
					AddField( "" );														// SHIPPING_CONTACT
					AddField( "" );														// SHIPPING_TELEPHONE
					AddField( "" );														// SHIPPING_EMAIL

					AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_DRIVER_NAME ] );		// DRIVER_NAME
					AddField( ImportLine[ (int)FIELDS.IDS_CALL_TAKER_ID ] );			// TRAILER_DRIVER_NAME

					AddField( ImportLine[ (int)FIELDS.IDS_WAYBILL ] );					//WAYBILL,

					EndOfLine();
				}

					// Build Trip Line
				AddField( SageImport.HEADER_COL.TRIP );									// Line Type Id
				AddField( AccountId );													// CUSTOMER_NAME
				AddField( Inv );														// INVOICE_NUMBER
				AddField( TripId );														// TRIP_ID

				AddField( ImportLine[ (int)FIELDS.IDS_SERVICE_LEVEL ] );				// SERVICE_LEVEL
				AddField( ImportLine[ (int)FIELDS.IDS_PACKAGE_TYPE ] );					// PACKAGE_TYPE
				AddField( ImportLine[ (int)FIELDS.IDS_PIECES ] );						// PIECES
				AddField( ImportLine[ (int)FIELDS.IDS_WEIGHT ] );						// WEIGHT
				AddField( ImportLine[ (int)FIELDS.IDS_DELIVERY_AMOUNT ] );				// PRICE
				AddField( ImportLine[ (int)FIELDS.IDS_INVOICE_TOTAL_INCLUDING_TAX ] );	// EXTENSION


						//  Get Taxes
				String	Tax1_Desc = "",
						Tax2_Desc = "",

						Tax1_Value = "0",
						Tax2_Value = "0",
						
						FuelSurcharge_Desc = "",
						FuelSurcharge_Value = "0";

				TAX_TYPE TaxState = TAX_TYPE.NONE;

				for( int I = (int)FIELDS.IDS_BEGIN_CHARGES, C = ImportLine.Count; I < C; I++ )
				{
					var Charge = ImportLine[ I ].Trim();
					if( Charge != "" )
					{
						var Parts = Charge.Split( Colons, StringSplitOptions.RemoveEmptyEntries );
						if( Parts.Length == 2 )
						{
							switch( Parts[ 0 ] )
							{
							case "CHG_FSG":			// Is Fuel Surcharge
							case "CHG_FSC":			// Is Fuel Surcharge
								TaxState = TAX_TYPE.FSG;
								goto DoTax;
							case "CHG_HST":			// Is Tax?
								TaxState = TAX_TYPE.TAX_1;
								goto DoTax;
							case "CHG_GST":
								TaxState = TAX_TYPE.TAX_2;
DoTax:							ImportLine[ I ] = "~~~";		// Something with no colons, skip later

								Parts[ 0 ] = Parts[ 0 ].Substring( 4 );		// Remove CHG_

								var Val = Parts[ 1 ].Trim();
								if( Val != "" )
								{
									switch( TaxState )
									{
									case TAX_TYPE.FSG:
										FuelSurcharge_Value = Val;
										FuelSurcharge_Desc = Parts[ 0 ];
										break;
									case TAX_TYPE.TAX_1:
										Tax1_Value = Val;
										Tax1_Desc = Parts[ 0 ];
										break;
									case TAX_TYPE.TAX_2:
										Tax2_Value = Val;
										Tax2_Desc = Parts[ 0 ];
										break;
									}
								}
								TaxState = TAX_TYPE.NONE;
								break;
							}
						}
					}
					else
						break;
				}

				AddField( FuelSurcharge_Desc );		//FUEL_SURCHARGE_ID,
				AddField( FuelSurcharge_Value );	//FUEL_SURCHARGE_VALUE,

				AddField( Tax1_Desc );				//TAX_ID_1,
				AddField( Tax1_Value );				//TAX_VALUE_1,
				AddField( Tax2_Desc );				//TAX_ID_2,
				AddField( Tax2_Value );				//TAX_VALUE_2,

				EndOfLine();

				for( int I = (int)FIELDS.IDS_BEGIN_CHARGES, C = ImportLine.Count; I < C; )
				{
					var Charge = ImportLine[ I++ ].Trim();
					if( Charge != "" )
					{
						var Parts = Charge.Split( Colons, StringSplitOptions.RemoveEmptyEntries );
						if( Parts.Length == 2 )
						{
							// Build Charge Lines
							AddField( SageImport.HEADER_COL.CHARGE_LINE );			// Line Type Id

							if( Parts[ 0 ].StartsWith( "CHG_" ) )
								Parts[ 0 ] = Parts[ 0 ].Substring( 4 );

							AddField( AccountId );								// CUSTOMER_NAME
							AddField( Inv );									// INVOICE_NUMBER
							AddField( LastTripId );								// TRIP_ID
							AddField( Parts[ 0 ] );								// DESCRIPTION
							AddField( Parts[ 1 ] );								// VALUE

							EndOfLine();
						}
					}
					else
						break;
				}
			}

			EndOfInvoice();

			DoDebugText( CsvFile, "After Re-Map" );

			return new MemoryStream( System.Text.Encoding.UTF8.GetBytes( CsvFile ) );
		}

		internal static Stream ReMapCsv( string Csv, bool debugMode, string errorPath )
		{
			using( MemoryStream Stream = new MemoryStream( System.Text.Encoding.UTF8.GetBytes( Csv ) ) )
				return ReMapCsv( Stream, debugMode, errorPath );
		}
	}
}
