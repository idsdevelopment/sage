﻿using System.ComponentModel.DataAnnotations;
using System;
using System.Runtime.Serialization;

namespace IDS_Sage.Database
{
	[Serializable()]
	public class Customer : ISerializable 
	{
		[Key]
		public string IdsAccount { get; set; }
		public string CustomerName { get; set; }
		public string Suite { get; set; }
		public string AddressLine1 { get; set; }
		public string AddressLine2 { get; set; }
		public string City { get; set; }
		public string Region { get; set; }
		public string PostCode { get; set; }
		public string Country { get; set; }
		public string ContactName { get; set; }
		public string EmailAddress { get; set; }
		public string Telephone { get; set; }
		public string Notes { get; set; }

		public Customer()
		{
		}

		public Customer( SerializationInfo info, StreamingContext context )
		{
			IdsAccount = info.GetString( "Ac" );
			CustomerName = info.GetString( "Cn" );
			Suite = info.GetString( "Su");
			AddressLine1 = info.GetString( "A1" );
			AddressLine2 = info.GetString( "A2" );
			City = info.GetString( "Ci" );
			Region = info.GetString( "Rg" );
			PostCode = info.GetString( "Po" );
			Country = info.GetString( "Co" );
			ContactName = info.GetString( "Ctn" );
			EmailAddress = info.GetString( "Em" );
			Telephone = info.GetString( "Te" );
			Notes = info.GetString( "No" );
		}

		public void GetObjectData( SerializationInfo info, StreamingContext context )
		{
			info.AddValue( "Ac", IdsAccount );
			info.AddValue( "Cn", CustomerName );
			info.AddValue( "Su", Suite );
			info.AddValue( "A1", AddressLine1 );
			info.AddValue( "A2", AddressLine2 );
			info.AddValue( "Ci", City );
			info.AddValue( "Rg", Region );
			info.AddValue( "Po", PostCode );
			info.AddValue( "Co", Country );
			info.AddValue( "Ctn", ContactName );
			info.AddValue( "Em", EmailAddress );
			info.AddValue( "Te", Telephone );
			info.AddValue( "No", Notes );
		}
	
		public Customer( string idsAccount, string customerName,
						 string suite, string addressLine1 = "", string addressLine2 = "",
						 string city = "", string region = "", string postCode = "",
						 string country = "", string contactName = "", string emailAddress = "",
						 string telephone = "", string notes = "" )
		{
			UpdateCustomer( idsAccount, customerName, suite, addressLine1, addressLine2, city, region, postCode, country,
							contactName, emailAddress, telephone, notes );
		}

		public void UpdateCustomer( string idsAccount, string customerName,
							string suite, string addressLine1 = "", string addressLine2 = "",
							string city = "", string region = "", string postCode = "",
							string country = "", string contactName = "", string emailAddress = "",
							string telephone = "", string notes = "" )
		{
			IdsAccount = idsAccount.Trim();
			CustomerName = customerName.Trim();
			Suite = suite.Trim();
			AddressLine1 = addressLine1.Trim();
			AddressLine2 = addressLine2.Trim();
			City = city.Trim();
			Region = region.Trim();
			PostCode = postCode.Trim();
			Country = country.Trim();
			ContactName = contactName.Trim();
			EmailAddress = emailAddress.Trim();
			Telephone = telephone.Trim();
			Notes = notes.Trim();
		}

		public void UpdateCustomer( Customer cust )
		{
			UpdateCustomer( cust.IdsAccount, cust.CustomerName, cust.Suite, cust.AddressLine1, cust.AddressLine2, cust.City, cust.Region, cust.PostCode, cust.Country );
		}
	}


}
