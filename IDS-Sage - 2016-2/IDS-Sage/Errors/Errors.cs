﻿using System;
using System.Windows.Forms;

namespace IDS_Sage.Errors
{
	public partial class ErrorsForm : Form
	{
		public ErrorsForm()
		{
			InitializeComponent();
		}

		private void button1_Click( object sender, EventArgs e )
		{
			Close();
		}

		public void Show( string errorText )
		{
			ErrorText.Text = errorText;
			ShowDialog();
		}
	}
}
