using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace IDS_Sage.IDS.Http
{
	public partial class HttpIo
	{
		public class ParameterEntry
		{
		private readonly bool isBinary;
		private readonly byte[] Data = null;
		private readonly string StringData = null;
		private readonly string _FileName = null;

			public ParameterEntry( byte[] data )
			{
				isBinary = true;
				Data = data;
			}

			public ParameterEntry( string stringOrFileName, bool isFile = false )
			{
				isBinary = isFile;

				if( isFile )
				{
					using( var Stream = new FileStream( _FileName = stringOrFileName, FileMode.Open, FileAccess.Read ) )
					{
						using( var Reader = new BinaryReader( Stream ) )
						{
							Data = Reader.ReadBytes( (int)Stream.Length );
						}
					}
				}
				else
					StringData = stringOrFileName;
			}

			public ParameterEntry( string fileName, MemoryStream Stream )
			{
				isBinary = true;
				Data = Stream.ToArray();
				StringData = fileName;
			}

	
			public ParameterEntry( string fileName, byte[] data )
			{
				isBinary = true;
				_FileName = fileName;
				Data = data;
			}

	
			public bool IsBinary
			{
				get
				{
					return isBinary;
				}
			}


			public bool IsFile
			{
				get
				{
					return isBinary && !string.IsNullOrEmpty( _FileName );
				}
			}

			public string FileName
			{
				get
				{
					return _FileName;
				}
			}

			public string AsString
			{
				get
				{
					return( isBinary ? Data == null ? "" : System.Text.Encoding.ASCII.GetString( Data ) : ( StringData ?? "" ) );
				}
			}

			public byte[] AsBytes
			{
				get
				{
					return( isBinary ? Data == null ? new byte[ 0 ] : Data : System.Text.Encoding.ASCII.GetBytes( StringData ?? "" ) );
				}
			}
		}

		public class Parameters : Dictionary<string, ParameterEntry>
		{
			public static string ValueAsString( KeyValuePair<string,ParameterEntry> Kvp )
			{
				return Kvp.Value.AsString;
			}

			public static string KeyAsString( KeyValuePair<string, ParameterEntry> Kvp )
			{
				return Kvp.Key;
			}

			public static string ValueAsHtmlString( KeyValuePair<string, ParameterEntry> Kvp )
			{
				return WebUtility.HtmlEncode( Kvp.Value.AsString );
			}

			public static string KeyAsHtmlString( KeyValuePair<string, ParameterEntry> Kvp )
			{
				return WebUtility.HtmlEncode( Kvp.Key );
			}

			public void Add( string key, byte[] data, bool isFile = false )
			{
				base.Add( key, ( isFile ? new ParameterEntry( key, data ) : new ParameterEntry( data ) ) );
			}

			public void Add( string key, string stringOrFileName, bool isFile = false )
			{
				base.Add( key, new ParameterEntry( stringOrFileName, isFile ) );
			}

			public void Add( string key, bool isFile = false )
			{
				base.Add( key, new ParameterEntry( key, isFile ) );
			}

			public void Add( string key, MemoryStream stream )
			{
				base.Add( key, new ParameterEntry( key, stream ) );
			}

			
			public override string ToString()
			{
				var RetVal = "";

				foreach( var Bytes in this )
				{
					if( RetVal != "" )
						RetVal += "&";

					RetVal += ( KeyAsHtmlString( Bytes ) + "=" + ValueAsHtmlString( Bytes ) );
				}

				return RetVal;
			}
		}

		public class RequestEntry : Parameters
		{
			internal Semaphore _Waiting = new Semaphore();

			public string Url;
			public string Content;
			public int Timeout;

			public bool IsMultiPart;
			public byte[] BinaryContent;

			public Action<RequestEntry> OnSuccess,
											  OnFail;

			public  CookieContainer Cookies = new CookieContainer();
			public string Error;

			public enum REQUEST_TYPE{ POST, GET };

			public REQUEST_TYPE Operation;

			internal class Semaphore
			{
				bool _Waiting;
				private readonly object LockObject = new object();

				internal bool Waiting
				{
					get
					{
						lock( LockObject )
							return _Waiting;
					}

					set
					{
						lock( LockObject )
							_Waiting = value;
					}
				}

				public static implicit operator Semaphore( bool value )
				{
					return new Semaphore{ Waiting = value };
				}


				public static implicit operator bool( Semaphore value )
				{
					return value.Waiting;
				}
			}

			public RequestEntry( string url, Action<RequestEntry> onSuccess, Action<RequestEntry> onFail, bool wait = false, int timeOut = INITIAL_TIMEOUT )
			{
				Url = url;
				OnSuccess = onSuccess;
				OnFail = onFail;
				Timeout = timeOut;
				Waiting = wait;

				lock( CookieLock )
				{
					if( GlobalCookies == null )
						InitCookie();

					Cookies = GlobalCookies;
				}
			}


			public bool Waiting
			{
				get
				{
					return _Waiting;
				}

				internal set
				{
					_Waiting.Waiting = value;
				}
			}
		}


		public class RequestQueue : Queue<RequestEntry>
		{
			private readonly object LockObject = new object();

			public void Add( RequestEntry entry )
			{
				lock( LockObject )
					Enqueue( entry );
			}

			public new int Count
			{
				get
				{
					lock( LockObject )
						return base.Count;
				}
			}

			public RequestEntry NextEntry {
				get
				{
					lock( LockObject )
						return Dequeue();
				}
			}
		}
	}
}