﻿using System;
using System.Collections.Generic;
using System.IO;
using IDS_Sage.Database;
// ReSharper disable AccessToModifiedClosure

namespace IDS_Sage.Import
{
	internal class SageImport : Ids_Sage
	{
		internal class ErrorEntry
		{
			internal string Error;
			internal int LineNumber;
		}

		internal class ErrorList : List<ErrorEntry>
		{
			internal void Add( string _Error, int _LineNumber )
			{
				base.Add( new ErrorEntry { Error = _Error, LineNumber = _LineNumber } );
			}

			public override string ToString()
			{
				var Retval = "";

				foreach( var Entry in this )
					Retval += "Line Number: " + Entry.LineNumber.ToString() + ",  Error: " + Entry.Error + "\r\n";

				return Retval;
			}

			internal void WriteToStream( Stream stream )
			{
				using( var Writer = new StreamWriter( stream, System.Text.Encoding.ASCII ) )
				{
					Writer.Write( ToString() );
					Writer.Flush();
					Writer.Close();
				}
			}
		}

		internal class HEADER_COL
		{
			internal const string ACCOUNT = "1",
									TRIP = "2",
									CHARGE_LINE = "3",		// Charges must be greater than trips for sort
									END_OF_INVOICE = "4";
		}

		internal class TRIP_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER,
				TRIP_ID,

				INVOICE_DATE,

				CALL_TIME,
				DELIVERY_DATE_TIME,

				PICKUP_TIME,
				DELIVERY_TIME,

				PICKUP_REFERENCE,
				PICKUP_COMPANY,
				PICKUP_ADDRESS_1,
				PICKUP_ADDRESS_2,
				PICKUP_CITY,
				PICKUP_REGION,
				PICKUP_COUNTRY,
				PICKUP_POST_CODE,
				PICKUP_ZONE,
				PICKUP_CONTACT,
				PICKUP_TELEPHONE,
				PICKUP_EMAIL,
				PICKUP_NOTES,

				DELIVERY_REFERENCE,
				DELIVERY_COMPANY,
				DELIVERY_ADDRESS_1,
				DELIVERY_ADDRESS_2,
				DELIVERY_CITY,
				DELIVERY_REGION,
				DELIVERY_POST_CODE,
				DELIVERY_ZONE,
				DELIVERY_COUNTRY,
				DELIVERY_CONTACT,
				DELIVERY_TELEPHONE,
				DELIVERY_EMAIL,
				DELIVERY_NOTES,

				BILLING_COMPANY,
				BILLING_ADDRESS_1,
				BILLING_ADDRESS_2,
				BILLING_CITY,
				BILLING_REGION,
				BILLING_POST_CODE,
				BILLING_COUNTRY,
				BILLING_CONTACT,
				BILLING_TELEPHONE,
				BILLING_EMAIL,

				SHIPPING_COMPANY,
				SHIPPING_ADDRESS_1,
				SHIPPING_ADDRESS_2,
				SHIPPING_CITY,
				SHIPPING_REGION,
				SHIPPING_POST_CODE,
				SHIPPING_COUNTRY,
				SHIPPING_CONTACT,
				SHIPPING_TELEPHONE,
				SHIPPING_EMAIL,

				DRIVER_NAME,
				CALL_TAKER_ID,

				WAYBILL
			}
		}

		internal class PACKAGE_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER,
				TRIP_ID,

				SERVICE_LEVEL,
				PACKAGE_TYPE,
				PIECES,
				WEIGHT,
				PRICE,
				EXTENSION,

				FUEL_SURCHARGE_ID,
				FUEL_SURCHARGE_VALUE,

				TAX_ID_1,
				TAX_VALUE_1,
				TAX_ID_2,
				TAX_VALUE_2
			}
		}

		internal class CHARGE_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER,
				TRIP_ID,

				DESCRIPTION,
				VALUE
			}
		}

		internal class END_OF_INVOICE_LINE
		{
			internal enum FIELDS
			{
				ACCOUNT_ID = 1,
				INVOICE_NUMBER
				// TRIP_ID gets fudged
			}
		}

		ErrorList Errors;
		string ErrorPath;
		readonly bool ByDriver;

		private bool HasErrors
		{
			get
			{
				return ( Errors.Count > 0 );
			}
		}


		public SageImport( string dbPath, string errorPath, string userName, string password, string salesLedger, string fuelSurchargeLedger, INVENTORY_CODE_TYPE invoiceType, bool ignoreCache )
			: base( dbPath, userName, password, salesLedger, fuelSurchargeLedger, invoiceType, ignoreCache )
		{
			ByDriver = ( invoiceType == INVENTORY_CODE_TYPE.BY_DRIVER );
			ErrorPath = Utils.AddPathSeparator( errorPath );
			Directory.CreateDirectory( errorPath );
		}

		private static int MaxEnum( Type enumType )
		{
			return Enum.GetValues( enumType ).GetUpperBound( 0 );
		}


		private static string FileNumberToString( int fieldNumber )
		{
			var Num = fieldNumber.ToString();
			fieldNumber--;

			var CsvCol = "";

			if( fieldNumber > 26 )
			{
				CsvCol = "A";
				fieldNumber %= 26;
			}

			CsvCol += ( (char)( (int)'A' + (int)fieldNumber ) ).ToString();

			return ( ":" + Num + "  (" + CsvCol + ")." );
		}

		public bool ImportCsv( bool ignoreAddress, Stream csvStream, bool debugMode, string errorPath, string cachePath, out int highestInvoiceNumber, int maxInvoiceNumber, out string errorText,
							   bool updateProjects )
		{
			errorPath = Utils.AddPathSeparator( errorPath );
			cachePath = Utils.AddPathSeparator( cachePath );

			highestInvoiceNumber = 0;
			errorText = "";

			var Csv = new CsvReader( csvStream );

			Action<string> DoDebug = ( fileName ) =>
			{
				if( debugMode )
				{
					try
					{
						using( var Stream = new FileStream( errorPath + fileName + Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
						{
							Csv.WriteToStream( Stream );
						}
					}
					catch
					{
					}
				}
			};

			CsvReader.Columns Cols = null;
			var CurrentRecordIndex = 0;

			Errors = new ErrorList();

			try
			{
				Action<int> IsValidDecimal = ( int FieldNumber ) =>
				{
					decimal Temp;
					if( Cols != null && !decimal.TryParse( Cols[ FieldNumber ].Trim(), out Temp ) )
						Errors.Add( "Invalid numeric field" + FileNumberToString( FieldNumber ), CurrentRecordIndex + 1 );
				};

				Action<int> IsNotBlank = ( int FieldNumber ) =>
				{
					if( string.IsNullOrEmpty( Cols[ FieldNumber ].Trim() ) )
						Errors.Add( "Field cannot be blank" +  FileNumberToString( FieldNumber ), CurrentRecordIndex + 1 );
				};


				Action<int> IsValidFieldCount = ( int FieldCount ) =>
				{
					if( Cols.Count < FieldCount )
						Errors.Add( "Invalid field count", CurrentRecordIndex + 1 );
				};

				Action<int> IsValidTaxId = ( int FieldNumber ) =>
				{
					switch( Cols[ FieldNumber ].Trim().ToUpper() )
					{
					case "HST":
					case "GST":
					case "PST":
						break;
					default:
						Errors.Add( "Invalid TaxId", CurrentRecordIndex + 1 );
						break;
					}
				};

				// Pre Parse Checking For Errors
				for( CurrentRecordIndex = 0; CurrentRecordIndex < Csv.Count; ++CurrentRecordIndex )
				{
					Cols = Csv[ CurrentRecordIndex ];
					if( Cols.Count > 0 )
					{
						switch( Cols[ 0 ].Trim() )
						{
						case HEADER_COL.ACCOUNT:
							IsValidFieldCount( MaxEnum( typeof( TRIP_LINE.FIELDS ) ) );
							IsNotBlank( (int)TRIP_LINE.FIELDS.ACCOUNT_ID );				// Must be in sage 
							IsNotBlank( (int)TRIP_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)TRIP_LINE.FIELDS.TRIP_ID );

							IsNotBlank( (int)TRIP_LINE.FIELDS.INVOICE_DATE );
							IsNotBlank( (int)TRIP_LINE.FIELDS.CALL_TIME );
							IsNotBlank( (int)TRIP_LINE.FIELDS.DELIVERY_DATE_TIME );
							break;

						case HEADER_COL.TRIP:
							IsValidFieldCount( MaxEnum( typeof( PACKAGE_LINE.FIELDS ) ) );
							IsNotBlank( (int)PACKAGE_LINE.FIELDS.ACCOUNT_ID );				// Must be in sage 
							IsNotBlank( (int)PACKAGE_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)PACKAGE_LINE.FIELDS.TRIP_ID );
							IsNotBlank( (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL );
							IsNotBlank( (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE );
							/*
														int QtyNdx = (int)TRIP_LINE.FIELDS.QUANTITY,
															WeightNdx = (int)TRIP_LINE.FIELDS.WEIGHT;

														String Qty = Cols[ QtyNdx ].Trim(),
																Weight = Cols[ WeightNdx ].Trim();

														bool NullQty = String.IsNullOrEmpty( Qty ),
															 NullWeight = String.IsNullOrEmpty( Weight );

														if( NullQty && NullWeight )
															Errors.Add( "Either A Quantity Or Weight Must Be Specified.", CurrentRecordIndex + 1 );

														if( !NullQty && !NullWeight )
															Errors.Add( "Cannot Have Both Quantity and Weight.", CurrentRecordIndex + 1 );

														if( !NullQty )
															IsValidDecimal( QtyNdx );

														if( !NullWeight )
															IsValidDecimal( WeightNdx );

							*/
							IsValidDecimal( (int)PACKAGE_LINE.FIELDS.PRICE );
							IsValidDecimal( (int)PACKAGE_LINE.FIELDS.EXTENSION );
							IsValidDecimal( (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_VALUE );
							IsValidDecimal( (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 );
							IsValidDecimal( (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 );
							break;

						case HEADER_COL.CHARGE_LINE:
							IsValidFieldCount( MaxEnum( typeof( CHARGE_LINE.FIELDS ) ) );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.ACCOUNT_ID );				// Must be in sage 
							IsNotBlank( (int)CHARGE_LINE.FIELDS.INVOICE_NUMBER );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.TRIP_ID );
							IsNotBlank( (int)CHARGE_LINE.FIELDS.DESCRIPTION );
							IsValidDecimal( (int)CHARGE_LINE.FIELDS.VALUE );
							break;

						case HEADER_COL.END_OF_INVOICE:
							Cols[ 3 ] = "~~~~~~~~~~~~~~~~~";			//Trip Id
							break;

						default:
							Errors.Add( "Unknown line type.", CurrentRecordIndex + 1 );
							break;
						}
					}
				}

				if( !HasErrors )
				{
					try
					{
						Csv.Sort( ( CsvReader.Columns a, CsvReader.Columns b ) =>
						{
							var RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ], b[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ] );
							if( RetVal == 0 )
							{
								RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ], b[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ] );
								if( RetVal == 0 )
								{
									RetVal = string.CompareOrdinal( a[ (int)TRIP_LINE.FIELDS.TRIP_ID ], b[ (int)TRIP_LINE.FIELDS.TRIP_ID ] );
									if( RetVal == 0 )
									{
										var a0 = a[ 0 ];

										RetVal = string.CompareOrdinal( a0, b[ 0 ] );		// Type
										if( RetVal == 0 )
										{
											switch( a0 )
											{
											case HEADER_COL.CHARGE_LINE:
												RetVal = string.CompareOrdinal( a[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ], b[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] );	// Group Same Taxes and Charges together
												break;
											case HEADER_COL.TRIP:
												RetVal = string.CompareOrdinal( a[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ], b[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ] );
												if( RetVal == 0 )
													RetVal = string.CompareOrdinal( a[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ], b[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] );
												break;
											}
										}
									}
								}
							}
							return RetVal;
						} );

						DoDebug( "After Internal Sort" );

						string LastColId = "",
								LastAccountId = "",
								LastTripId = "";

						CsvReader.Columns SaveCol = null;


						// Summarise Trips & Charges
						for( CurrentRecordIndex = Csv.Count - 1; CurrentRecordIndex >= 0; CurrentRecordIndex-- )
						{
							var Col = Csv[ CurrentRecordIndex ];
							var ColId = Col[ 0 ];
							var AccountId = Col[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ];
							var TripId = Col[ (int)TRIP_LINE.FIELDS.TRIP_ID ];

							if( LastColId == ColId && LastAccountId == AccountId && LastTripId == TripId )
							{
								switch( ColId )
								{
								case HEADER_COL.CHARGE_LINE:
								case HEADER_COL.TRIP:
									if( SaveCol == null )
										SaveCol = Csv[ CurrentRecordIndex + 1 ];

									if( ColId == HEADER_COL.CHARGE_LINE )
									{
										if( SaveCol[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] == Col[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ] )	// Same Charge
										{
											var Val = double.Parse( SaveCol[ (int)CHARGE_LINE.FIELDS.VALUE ] ) + double.Parse( Col[ (int)CHARGE_LINE.FIELDS.VALUE ] );
											SaveCol[ (int)CHARGE_LINE.FIELDS.VALUE ] = Val.ToString( "0.##" );
											Csv.RemoveAt( CurrentRecordIndex );
											continue;
										}
										SaveCol = null;
									}
									else
									{
										if( SaveCol[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ] == Col[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ]
											&& SaveCol[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] == Col[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ] )	// Same Charge
										{
											var SaveColQty = SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ].Trim();
											var ColQty = Col[ (int)PACKAGE_LINE.FIELDS.PIECES ].Trim();

											double Val;
											if( SaveColQty != "" && ColQty != "" )
											{
												Val = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.PIECES ] );
												SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] = Val.ToString( "0.##" );
											}
											else
												SaveCol[ (int)PACKAGE_LINE.FIELDS.PIECES ] = "";


											var SaveColWeight = SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ].Trim();
											var ColWeight = Col[ (int)PACKAGE_LINE.FIELDS.WEIGHT ].Trim();

											if( SaveColWeight != "" && ColWeight != "" )
											{
												Val = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] );
												SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] = Val.ToString( "0.##" );
											}
											else
												SaveCol[ (int)PACKAGE_LINE.FIELDS.WEIGHT ] = "";

											Val = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] );
											SaveCol[ (int)PACKAGE_LINE.FIELDS.EXTENSION ] = Val.ToString( "0.##" );


											Val = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] );
											SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ] = Val.ToString( "0.##" );

											Val = double.Parse( SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] ) + double.Parse( Col[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] );
											SaveCol[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ] = Val.ToString( "0.##" );

											Csv.RemoveAt( CurrentRecordIndex );
											continue;
										}
										SaveCol = null;
									}
									break;
								default:
									SaveCol = null;
									break;
								}
							}
							else
								SaveCol = null;

							LastColId = ColId;
							LastAccountId = AccountId;
							LastTripId = TripId;
						}

						DoDebug( "After Summarise (Before Import)" );

						var InvoiceDetails = new UpdateObject.TripDetailsList();
						UpdateObject.TripDetails CurrentAccount = null;
						UpdateObject.TripPackageDetails CurrentPackageDetails = null;

						var MangledCodes = GetAllCustomersCodesPacked();

						var CustDb = new DbCustomers();
						CustDb.Open( cachePath );

						try
						{
							var CurrentInvoiceNumber = 0;
							for( CurrentRecordIndex = 0; CurrentRecordIndex < Csv.Count; ++CurrentRecordIndex )
							{
								try
								{
									Cols = Csv[ CurrentRecordIndex ];

									switch( Cols[ 0 ].Trim() )
									{
									case HEADER_COL.ACCOUNT:
										var InvoiceNumber = Cols[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ];
										var AccountId = Cols[ (int)TRIP_LINE.FIELDS.ACCOUNT_ID ];
										var BillingCompany = Cols[ (int)TRIP_LINE.FIELDS.BILLING_COMPANY ];

										var MangledCode = DbCustomers.PackCode( BillingCompany );
										if( MangledCodes.ContainsKey( MangledCode ) )
											BillingCompany = MangledCodes[ MangledCode ];		// Key From Sage Database

										var AddressLine1 = Cols[ (int)TRIP_LINE.FIELDS.BILLING_ADDRESS_1 ];
										var AddressLine2 = Cols[ (int)TRIP_LINE.FIELDS.BILLING_ADDRESS_2 ];
										var City = Cols[ (int)TRIP_LINE.FIELDS.BILLING_CITY ];
										var Region = Cols[ (int)TRIP_LINE.FIELDS.BILLING_REGION ];
										var PostalCode = Cols[ (int)TRIP_LINE.FIELDS.BILLING_POST_CODE ];
										var Country = Cols[ (int)TRIP_LINE.FIELDS.BILLING_COUNTRY ];
										var Contact = Cols[ (int)TRIP_LINE.FIELDS.BILLING_CONTACT ];
										var Telephone = Cols[ (int)TRIP_LINE.FIELDS.BILLING_TELEPHONE ];
										var EmailAddress = Cols[ (int)TRIP_LINE.FIELDS.BILLING_EMAIL ];

										CurrentAccount = new UpdateObject.TripDetails( AccountId, InvoiceNumber, Cols[ (int)TRIP_LINE.FIELDS.INVOICE_DATE ],
																					Cols[ (int)TRIP_LINE.FIELDS.TRIP_ID ], Cols[ (int)TRIP_LINE.FIELDS.DRIVER_NAME ], Cols[ (int)TRIP_LINE.FIELDS.CALL_TAKER_ID ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_TIME ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_COMPANY ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_REFERENCE ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_ADDRESS_1 ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_ADDRESS_2 ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_CITY ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_REGION ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_POST_CODE ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_COUNTRY ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_CONTACT ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_TELEPHONE ],
																					Cols[ (int)TRIP_LINE.FIELDS.PICKUP_EMAIL ], Cols[ (int)TRIP_LINE.FIELDS.PICKUP_NOTES ],

																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_TIME ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_COMPANY ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_REFERENCE ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_ADDRESS_1 ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_ADDRESS_2 ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_CITY ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_REGION ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_POST_CODE ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_COUNTRY ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_CONTACT ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_TELEPHONE ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_EMAIL ], Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_NOTES ],

																					BillingCompany,
																					AddressLine1, AddressLine2,
																					City, Region, PostalCode, Country,
																					Contact, Telephone, EmailAddress,

																					Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_COMPANY ],
																					Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_ADDRESS_1 ], Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_ADDRESS_2 ],
																					Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_CITY ], Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_REGION ],
																					Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_POST_CODE ], Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_COUNTRY ],
																					Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_CONTACT ], Cols[ (int)TRIP_LINE.FIELDS.SHIPPING_TELEPHONE ],
																					Cols[ (int)TRIP_LINE.FIELDS.DELIVERY_EMAIL ], Cols[ (int)TRIP_LINE.FIELDS.WAYBILL ]
																);
										InvoiceDetails.Add( CurrentAccount );

										if( !int.TryParse( Cols[ (int)TRIP_LINE.FIELDS.INVOICE_NUMBER ], out CurrentInvoiceNumber ) )
											CurrentInvoiceNumber = 0;

										highestInvoiceNumber = Math.Max( CurrentInvoiceNumber, highestInvoiceNumber );

										if( CurrentInvoiceNumber > maxInvoiceNumber )
											return !HasErrors;

										var Cust = new Customer( AccountId, BillingCompany, "", AddressLine1, AddressLine2,
																 City, Region, PostalCode, Country,
																 Contact, EmailAddress, Telephone, "" );


										if( !ByDriver )
										{
											string OldCustCode;
											if( CustDb.Update( ignoreAddress, Cust, out OldCustCode ) && !IgnoreCache )
												UpdateCustomer( OldCustCode, Cust );
										}
										break;

									case HEADER_COL.TRIP:
										CurrentPackageDetails = new UpdateObject.TripPackageDetails( Cols[ (int)PACKAGE_LINE.FIELDS.SERVICE_LEVEL ], Cols[ (int)PACKAGE_LINE.FIELDS.PACKAGE_TYPE ],
																									Cols[ (int)PACKAGE_LINE.FIELDS.PIECES ], Cols[ (int)PACKAGE_LINE.FIELDS.WEIGHT ],
																									Cols[ (int)PACKAGE_LINE.FIELDS.PRICE ], Cols[ (int)PACKAGE_LINE.FIELDS.EXTENSION ],
																									Cols[ (int)PACKAGE_LINE.FIELDS.TAX_ID_1 ], Cols[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_1 ],
																									Cols[ (int)PACKAGE_LINE.FIELDS.TAX_ID_2 ], Cols[ (int)PACKAGE_LINE.FIELDS.TAX_VALUE_2 ],
																									Cols[ (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_ID ], Cols[ (int)PACKAGE_LINE.FIELDS.FUEL_SURCHARGE_VALUE ] );

										CurrentAccount.Packages.Add( CurrentPackageDetails );
										break;

									case HEADER_COL.CHARGE_LINE:
										var Charge = new UpdateObject.ChargeDetails( Cols[ (int)CHARGE_LINE.FIELDS.DESCRIPTION ], Cols[ (int)CHARGE_LINE.FIELDS.VALUE ] );
										CurrentPackageDetails.Charges.Add( Charge );
										break;

									case HEADER_COL.END_OF_INVOICE:
										try
										{
											Update( new UpdateObject( INVENTORY_CODE_TYPE.BY_SERVICE_PACKAGE, InvoiceDetails  ), updateProjects );
										}
										finally
										{
											InvoiceDetails = new UpdateObject.TripDetailsList();
											CurrentAccount = null;
											CurrentPackageDetails = null;
										}
										CurrentInvoiceNumber = 0;
										break;
									}
								}
								catch( Exception E )
								{
									Errors.Add( "Error On Invoice " + CurrentInvoiceNumber.ToString() + ": (" + E.Message + ")", CurrentRecordIndex + 1 );
									CurrentInvoiceNumber = 0;
								}
							}
						}
						finally
						{
							CustDb.Close();
						}
					}
					catch( Exception E )
					{
						Errors.Add( "Exception Error: (" + E.Message + ")", CurrentRecordIndex + 1 );
					}
				}
				else
					return false;
			}
			finally
			{
				if( HasErrors )
				{
					var Id = DateTime.Now.ToString( "yyyyMMddHHmmss" );

					using( var Stream = new FileStream( errorPath + Id +  Globals.ERROR_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
					{
						errorText = Errors.ToString();
						Errors.WriteToStream( Stream );
					}

					using( var Stream = new FileStream( errorPath + Id +  Globals.CSV_EXTENSION, FileMode.Create, FileAccess.ReadWrite ) )
					{
						Csv.WriteToStream( Stream );
					}
				}
			}
			return !HasErrors;
		}

		public bool ImportCsv( bool ignoreAddress, string fileName, bool debugMode, string errorPath, string cachePath, out int highestInvoiceNumber, int maxInvoiceNumber, out string errorText, bool updateProjects )
		{
			return ImportCsv( ignoreAddress, new FileStream( fileName, FileMode.Open, FileAccess.Read ), debugMode, errorPath, cachePath, out highestInvoiceNumber, maxInvoiceNumber, out errorText, updateProjects );
		}
	}
}
